package ru.clevertec.sevenlis.lesson03;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ContactInfo {
    @PrimaryKey
    public long _id;

    @ColumnInfo(name = "first_name")
    public String firstName;

    @ColumnInfo(name = "last_name")
    public String lastName;

    @ColumnInfo(name = "phone_number")
    public String phoneNumber;

    @ColumnInfo(name = "e_mail")
    public String eMail;

    @ColumnInfo(name = "displayed_name")
    public String displayedName;

    public ContactInfo(long _id, String firstName, String lastName, String displayedName, String phoneNumber, String eMail) {
        this._id = _id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayedName = displayedName;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
    }

    public long get_id() {
        return _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return eMail;
    }

    public String getDisplayedName() {
        return displayedName;
    }
}
