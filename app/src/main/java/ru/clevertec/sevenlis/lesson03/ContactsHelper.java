package ru.clevertec.sevenlis.lesson03;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContactsHelper {
    public static final String CONTACT_ID_SP_KEY = "CONTACT_ID_SP_KEY";

    public static List<ContactInfo> getAllContactsLocal(Context mContext) {
        return DBHelper.getInstance(mContext).contactInfoDao().getAll();
    }

    public static ContactInfo getContactInfoLocal(Context mContext, long contactId) {
        return DBHelper.getInstance(mContext).contactInfoDao().getById(contactId);
    }

    public static ContactInfo getContactInfo(Context mContext, Uri contactUri) {
        ContactInfo contactInfo = null;

        final String[] projection = new String[] {
                ContactsContract.Contacts._ID
        };

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactsCursor = contentResolver.query(contactUri, projection, null, null, null, null);
        if (contactsCursor.moveToFirst()) {
            long contactId = contactsCursor.getLong(contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            contactInfo = ContactsHelper.getContactInfo(mContext, contactId);
        }
        contactsCursor.close();

        return contactInfo;
    }

    public static ContactInfo getContactInfo(Context mContext, long contactId) {
        ContactInfo contactInfo = null;

        final String[] projection = new String[] {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        };

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactsCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projection, ContactsContract.Contacts._ID + "=?", new String[] { String.valueOf(contactId) }, null, null);
        if (contactsCursor.moveToFirst()) {
            String contact_id = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            String displayed_name = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));

            HashMap<String, String> hashMap = getName(contentResolver, contact_id);
            String contact_first_name = hashMap.get("first_name");
            String contact_last_name = hashMap.get("last_name");

            StringBuilder contact_email_sb = new StringBuilder();
            for (String s : getEmail(contentResolver, contact_id)) {
                contact_email_sb.append(s).append("\n");
            }

            StringBuilder contact_phone_sb = new StringBuilder();
            for (String s : getPhone(contentResolver, contact_id)) {
                contact_phone_sb.append(s).append("\n");
            }

            contactInfo = new ContactInfo(
                    Long.parseLong(contact_id),
                    contact_first_name,
                    contact_last_name,
                    displayed_name,
                    contact_phone_sb.toString(),
                    contact_email_sb.toString()
            );
        }
        contactsCursor.close();

        return contactInfo;
    }

    public static List<ContactInfo> getAllContacts(Context mContext) {
        List<ContactInfo> contactInfoList = new ArrayList<>();
        final String[] projection = new String[] {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        };
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactsCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projection, null, null, null);
        while (contactsCursor.moveToNext()) {
            String contact_id = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            String displayed_name = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));

            HashMap<String, String> hashMap = getName(contentResolver, contact_id);
            String contact_first_name = hashMap.get("first_name");
            String contact_last_name = hashMap.get("last_name");

            StringBuilder contact_email_sb = new StringBuilder();
            for (String s : getEmail(contentResolver, contact_id)) {
                contact_email_sb.append(s).append("\n");
            }

            StringBuilder contact_phone_sb = new StringBuilder();
            for (String s : getPhone(contentResolver, contact_id)) {
                contact_phone_sb.append(s).append("\n");
            }

            contactInfoList.add(new ContactInfo(
                    Long.parseLong(contact_id),
                    contact_first_name,
                    contact_last_name,
                    displayed_name,
                    contact_phone_sb.toString(),
                    contact_email_sb.toString() )
            );
        }
        contactsCursor.close();

        return contactInfoList;
    }

    private static HashMap<String, String> getName(ContentResolver contentResolver, String contact_id) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("first_name", "");
        hashMap.put("last_name", "");

        final String[] projection = new String[] {
                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME
        };
        final Cursor cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projection, ContactsContract.Data.CONTACT_ID + "=?", new String[] { contact_id }, null);
        if (cursor.moveToFirst()) {
            hashMap.put("first_name", cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)));
            hashMap.put("last_name", cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)));
        }
        cursor.close();

        return hashMap;
    }

    private static List<String> getEmail(ContentResolver contentResolver, String contact_id) {
        List<String> list = new ArrayList<>();

        final String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.TYPE
        };
        final Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, projection, ContactsContract.Data.CONTACT_ID + "=?", new String[] { contact_id }, null);
        while (cursor.moveToNext()) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Email.ADDRESS));
            if (!list.contains(string)) {
                list.add(string);
            }
        }
        cursor.close();

        return list;
    }

    private static List<String> getPhone(ContentResolver contentResolver, String contact_id) {
        List<String> list = new ArrayList<>();

        final String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.TYPE
        };
        final Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, ContactsContract.Data.CONTACT_ID + "=?", new String[] { contact_id }, null);
        while (cursor.moveToNext()) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (!list.contains(string)) {
                list.add(string);
            }
        }
        cursor.close();

        return list;
    }

    public interface ContactInfoSelectListener {
        void onContactInfoSelected(ContactInfo contactInfo);
    }
}
