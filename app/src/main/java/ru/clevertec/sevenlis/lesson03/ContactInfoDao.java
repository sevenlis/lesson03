package ru.clevertec.sevenlis.lesson03;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ContactInfoDao {
    @Query("SELECT * FROM contactinfo")
    List<ContactInfo> getAll();

    @Query("SELECT * FROM contactinfo WHERE _id IN (:ids)")
    List<ContactInfo> getByIds(int[] ids);

    @Query("SELECT * FROM contactinfo WHERE first_name LIKE :filter OR last_name LIKE :filter")
    List<ContactInfo> findContactInfo(String filter);

    @Query("SELECT * FROM contactinfo WHERE _id = :id LIMIT 1")
    ContactInfo getById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ContactInfo... contactInfos);

    @Delete
    void deleteAll(ContactInfo... contactInfos);
}
