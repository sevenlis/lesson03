package ru.clevertec.sevenlis.lesson03;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;

public class ContactInfoSelectDialogFragment extends DialogFragment {
    private final List<ContactInfo> contactInfoList = new ArrayList<>();
    private ContactsHelper.ContactInfoSelectListener onContactInfoSelectListener;
    private Context mContext;

    public ContactInfoSelectDialogFragment() {}

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.onContactInfoSelectListener = (ContactsHelper.ContactInfoSelectListener) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contacts_list_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ContactInfoListAdapter contactInfoListAdapter = new ContactInfoListAdapter(mContext, contactInfoList);
        ListView contactsListView = view.findViewById(android.R.id.list);
        contactsListView.setAdapter(contactInfoListAdapter);
        contactsListView.setOnItemClickListener((adapterView, itemView, i, l) -> {
            onContactInfoSelectListener.onContactInfoSelected(contactInfoList.get(i));
            dismiss();
        });

        contactInfoList.addAll(ContactsHelper.getAllContactsLocal(mContext));
    }

}
