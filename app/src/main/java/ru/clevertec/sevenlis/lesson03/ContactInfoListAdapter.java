package ru.clevertec.sevenlis.lesson03;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ContactInfoListAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<ContactInfo> contactInfoList;

    public ContactInfoListAdapter(Context mContext, List<ContactInfo> contactInfoList) {
        this.mContext = mContext;
        this.contactInfoList = contactInfoList;
    }

    @Override
    public int getCount() {
        return contactInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return contactInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((ContactInfo) getItem(i)).get_id();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.contacts_list_item, viewGroup, false);

        ContactInfo item = (ContactInfo) getItem(i);

        TextView tvName = view.findViewById(android.R.id.text1);
        tvName.setText(item.getDisplayedName());

        return view;
    }
}
