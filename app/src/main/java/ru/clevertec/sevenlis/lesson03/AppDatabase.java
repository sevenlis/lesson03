package ru.clevertec.sevenlis.lesson03;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ContactInfo.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ContactInfoDao contactInfoDao();
}
