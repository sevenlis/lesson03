package ru.clevertec.sevenlis.lesson03;

import android.content.Context;

import androidx.room.Room;

public class DBHelper {
    static volatile AppDatabase db;

    static AppDatabase getInstance(Context context) {
        if (db != null) return db;

        synchronized (AppDatabase.class) {
            if (db == null)
                db = Room.databaseBuilder(context, AppDatabase.class, "lesson03_db.s3db").allowMainThreadQueries().build();
        }

        return db;
    }
}
