package ru.clevertec.sevenlis.lesson03;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements ContactsHelper.ContactInfoSelectListener {
    public static final int REQUEST_READ_CONTACTS_PERMISSION = Math.abs("REQUEST_READ_CONTACTS_PERMISSION".hashCode());

    private static final int SELECT_CONTACT_REQUEST_CODE = Math.abs("SELECT_CONTACT_REQUEST_CODE".hashCode());
    private static final String CHANNEL_ID = MainActivity.class.getSimpleName();

    private ContactInfo contactInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            long _id = savedInstanceState.getLong("contactInfo_id",-1);
            contactInfo = DBHelper.getInstance(this).contactInfoDao().getById(_id);
        }

        AppCompatButton buttonSelectContact = findViewById(R.id.buttonSelectContact);
        buttonSelectContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    showContactsSelectionDialog();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{ Manifest.permission.READ_CONTACTS }, REQUEST_READ_CONTACTS_PERMISSION);
                }
            }
        });

        AppCompatButton buttonShowContacts = findViewById(R.id.buttonShowContacts);
        buttonShowContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ContactInfoSelectDialogFragment().show(getSupportFragmentManager(), null);
            }
        });

        AppCompatButton buttonShowContactSp = findViewById(R.id.buttonShowContactSp);
        buttonShowContactSp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
                contactInfo = ContactsHelper.getContactInfoLocal(MainActivity.this, preferences.getLong(ContactsHelper.CONTACT_ID_SP_KEY, -1));
                //showContactInfo();
                showContactInfoSnackBar();
            }
        });

        AppCompatButton buttonShowContactNotification = findViewById(R.id.buttonShowContactNotification);
        buttonShowContactNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
                contactInfo = ContactsHelper.getContactInfoLocal(MainActivity.this, preferences.getLong(ContactsHelper.CONTACT_ID_SP_KEY, -1));
                showContactInfoNotification();
            }
        });

        showContactInfo();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (contactInfo != null) {
            outState.putLong("contactInfo_id", contactInfo.get_id());
        }
    }

    private void showContactInfoNotification() {
        if (contactInfo != null) {
            RemoteViews layout = new RemoteViews(getPackageName(), R.layout.contact_info);
            layout.setTextViewText(R.id.textViewName, contactInfo.getDisplayedName());
            layout.setTextViewText(R.id.textViewPhone, contactInfo.getPhoneNumber());
            layout.setTextViewText(R.id.textViewEmail, contactInfo.getEmail());

            Notification notification;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                notification = getNotificationBuilder()
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                        .setCustomContentView(layout)
                        .setCustomBigContentView(layout)
                        .build();
            } else {
                String sContent = "\n" + contactInfo.getPhoneNumber() + "\n" + contactInfo.getEmail();

                NotificationCompat.Style bigTextStyle = new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(contactInfo.getDisplayedName())
                        .bigText(sContent);
                notification = getNotificationBuilder()
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setStyle(bigTextStyle)
                        .build();
            }

            getNotificationManager().notify(0, notification);
        }
    }

    private NotificationManagerCompat getNotificationManager() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_NAME = "Contact Info";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setVibrationPattern(new long[]{0});
            channel.enableVibration(false);
            notificationManager.createNotificationChannel(channel);
        }
        return notificationManager;
    }

    @SuppressWarnings("deprecation")
    private NotificationCompat.Builder getNotificationBuilder() {
        NotificationCompat.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(this, CHANNEL_ID);
            builder.setChannelId(CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(this);
        }
        return builder;
    }

    private void showContactInfo() {
        LinearLayout contactInfoLayout = findViewById(R.id.contactInfoLayout);
        contactInfoLayout.setVisibility(contactInfo == null ? View.INVISIBLE : View.VISIBLE);
        if (contactInfo != null) {
            TextView textViewName = findViewById(R.id.textViewName);
            textViewName.setText(contactInfo.getDisplayedName());

            TextView textViewPhone = findViewById(R.id.textViewPhone);
            textViewPhone.setText(contactInfo.getPhoneNumber());

            TextView textViewEmail = findViewById(R.id.textViewEmail);
            textViewEmail.setText(contactInfo.getEmail());
        }
    }

    private void showContactInfoSnackBar() {
        if (contactInfo != null) {
            LinearLayout contactInfoLayout = findViewById(R.id.contactInfoLayout);
            contactInfoLayout.setVisibility(View.INVISIBLE);

            final Snackbar snackbar = Snackbar.make(contactInfoLayout,"", Snackbar.LENGTH_INDEFINITE);
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

            final TextView sMessageTextView = layout.findViewById(com.google.android.material.R.id.snackbar_text);
            sMessageTextView.setVisibility(View.INVISIBLE);

            View snackBarView = getLayoutInflater().inflate(R.layout.contact_info, layout, false);
            snackBarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });

            TextView textViewName = snackBarView.findViewById(R.id.textViewName);
            textViewName.setText(contactInfo.getDisplayedName());

            TextView textViewPhone = snackBarView.findViewById(R.id.textViewPhone);
            textViewPhone.setText(contactInfo.getPhoneNumber());

            TextView textViewEmail = snackBarView.findViewById(R.id.textViewEmail);
            textViewEmail.setText(contactInfo.getEmail());

            layout.setPadding(0,0,0,0);

            layout.addView(snackBarView, 0);

            snackbar.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_CONTACTS_PERMISSION && grantResults.length > 0)
            showContactsSelectionDialog();
    }

    @SuppressWarnings("deprecation")
    void showContactsSelectionDialog() {
        //new ContactsSelectDialogFragment().show(getSupportFragmentManager(), null);
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, SELECT_CONTACT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_CONTACT_REQUEST_CODE && data != null) {
            ContactInfo contactInfo = ContactsHelper.getContactInfo(this, data.getData());
            if (contactInfo != null) {
                DBHelper.getInstance(this).contactInfoDao().insertAll(contactInfo);
                Toast.makeText(this, "Сохранение успешно", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onContactInfoSelected(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
        showContactInfo();

        SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
        editor.putLong(ContactsHelper.CONTACT_ID_SP_KEY, contactInfo.get_id()).apply();
    }
}